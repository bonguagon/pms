﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using SharpGL;
using SharpGL.Enumerations;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PMS_3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Triangle> triangles = new List<Triangle>();
        private OpenGL gl;
        private double a = 1;
        private double b = 0;
        private double rotateAngle = 0;
        private double move_x = 0.0;
        private double x = 0.0;
        private double y = 0.0;
        private double z = 0.0;
        private string sign = "+";
        private bool toRotate = false;
        private bool toMove = false;

        public MainWindow()
        {
            InitializeComponent();
        }
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {


            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.ClearColor(1, 1, 1, 1);

            gl.LoadIdentity();

            gl.MatrixMode(MatrixMode.Modelview);
            gl.LoadIdentity();

            gl.PushMatrix();
            try
            {
                a = Convert.ToDouble(textBox.Text);
            }
            catch (Exception)
            {
                a = 1.0;
            }
            try
            {
                b = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {
                b = 0;
            }

            if (triangles.Count != 0)
            {
                x = -(triangles[0].Points[0] + triangles[0].Points[2] + triangles[0].Points[4]) / 3.0;
                y = -(triangles[0].Points[1] + triangles[0].Points[3] + triangles[0].Points[5]) / 3.0;
            }
            if (toMove)
            {
                double move_y = a * move_x + b;
                gl.Translate(move_x, move_y, z);
            }

            if (toRotate)
            {
                gl.Translate(-x, -y, -z);
                gl.Rotate(rotateAngle, 0, 0, 1);
                gl.Translate(x, y, z);
            }

            foreach (var tr in triangles)
            {
                DrawTriangle(gl, tr);
            }

            gl.PopMatrix();

            gl.Flush();

            if (sign == "+")
            {
                move_x += 0.1f;
            }
            if (sign == "-")
            {
                move_x -= 0.1f;
            }

            if (move_x > 1.0)
            {
                sign = "-";
            }
            if (move_x < -1.0)
            {
                sign = "+";
            }

            rotateAngle += 3;
        }

        private void DrawTriangle(OpenGL gl, Triangle tr)
        {
            gl.Begin(OpenGL.GL_TRIANGLES);

            gl.Color(tr.Color.ScR, tr.Color.ScG, tr.Color.ScB);

            for (int i = 0; i < 5; i += 2)
            {
                gl.Vertex(tr.Points[i], tr.Points[i + 1]);
            }

            gl.End();

        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            gl = args.OpenGL;
            gl.MatrixMode(MatrixMode.Projection);
            gl.LoadIdentity();
            gl.Ortho(0, Width, 0, Height, -1, 1);
        }

        private void OpenGLControl_Resized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGLControl_OpenGLDraw(sender, args);
        }

        private double GetPosition(Random random)
        {
            return random.NextDouble() * (random.NextDouble() > 0.5 ? 1 : -1);
        }

        private void button_Click_1(object sender, RoutedEventArgs e)
        {
            if (triangles.Count == 1)
            {
                return;
            }
            var rand = new Random();
            var r = Convert.ToSingle(rand.NextDouble());
            var g = Convert.ToSingle(rand.NextDouble());
            var b = Convert.ToSingle(rand.NextDouble());

            var tr = new Triangle(GetPosition(rand), GetPosition(rand), GetPosition(rand), GetPosition(rand), GetPosition(rand), GetPosition(rand), r, g, b);

            triangles.Add(tr);
        }

        private void button1_Click_1(object sender, RoutedEventArgs e)
        {
            int n = triangles.Count;
            if (n != 0)
            {
                triangles.RemoveAt(triangles.Count - 1);
            }
        }

        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            toMove = !toMove;
        }

        private void checkBox_Copy_Checked(object sender, RoutedEventArgs e)
        {
            toRotate = !toRotate;
        }
    }

    class Triangle
    {
        private Color color;
        private double[] points;

        Triangle() { }

        public Triangle(double x1, double y1, double x2, double y2, double x3, double y3, float r, float g, float b)
        {
            color = new Color();
            color.ScR = r;
            color.ScG = g;
            color.ScB = b;

            points = new double[6];
            points[0] = x1;
            points[1] = y1;
            points[2] = x2;
            points[3] = y2;
            points[4] = x3;
            points[5] = y3;
        }

        public double[] Points
        {
            get { return points; }
        }

        public Color Color
        {
            get { return color; }
        }
    }
}

